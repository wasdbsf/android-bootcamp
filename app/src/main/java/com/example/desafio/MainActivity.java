package com.example.desafio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Pergunta pergunta;
    Button btn_atualizar, btn_verdadeiro, btn_falso, btn_resultado;
    TextView tvPergunta;
    int atual = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActiveAndroid.initialize(this);
        setContentView(R.layout.activity_main);
        inicializa_variavel();
        resetar();
    }


    public void inicializa_variavel() {
        btn_atualizar = (Button) findViewById(R.id.btn_atualizar);
        btn_atualizar.setOnClickListener(this);

        btn_verdadeiro = (Button) findViewById(R.id.btn_verdadeiro);
        btn_verdadeiro.setOnClickListener(this);

        btn_falso = (Button) findViewById(R.id.btn_falso);
        btn_falso.setOnClickListener(this);

        btn_resultado = (Button) findViewById(R.id.btn_resultado);
        btn_resultado.setOnClickListener(this);
        tvPergunta = (TextView) findViewById(R.id.tv_pergunta);
    }

    public void proxima_pergunta() {

        if (atual <= 8) {
            pergunta = new Pergunta().find(atual);
            tvPergunta.setText(String.valueOf(atual) + " - " + pergunta.descricao);
        } else {
            btn_verdadeiro.setVisibility(View.INVISIBLE);
            btn_falso.setVisibility(View.INVISIBLE);
            btn_resultado.setVisibility(View.VISIBLE);
            tvPergunta.setVisibility(View.INVISIBLE);
            Toast.makeText(this, getString(R.string.finalizou), Toast.LENGTH_LONG).show();
        }
    }


    public void resetar() {
        atual = 1;
        Pergunta.inserir();
        proxima_pergunta();
    }

    public String processar_resposta(int acao_ṕergunta, int acao_botao) {
        if (acao_botao == acao_ṕergunta) {
            Toast.makeText(this, getString(R.string.acertou), Toast.LENGTH_LONG).show();
            return "acertou";
        } else {
            Toast.makeText(this, getString(R.string.errou), Toast.LENGTH_LONG).show();
            return "errou";
        }
    }

    @Override
    public void onClick(View v) {
        if (v == btn_atualizar) {
            resetar();
            Toast.makeText(this, getString(R.string.atualizar), Toast.LENGTH_LONG).show();
        }
        if (v == btn_verdadeiro) {
            processar(1);

        }
        if (v == btn_falso) {
            processar(0);
        }

        if (v == btn_resultado) {
            startActivity(new Intent(this, resultado.class));
        }

    }

    public void processar(int btn_selecionado) {
        pergunta = new Pergunta().find(atual);
        pergunta.escolhido = btn_selecionado;
        pergunta.resultado = processar_resposta(pergunta.resposta, btn_selecionado);
        pergunta.save();
        atual = pergunta.ordenacao + 1;
        proxima_pergunta();
    }


}