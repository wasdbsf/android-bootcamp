package com.example.desafio;

import android.database.Cursor;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "pergunta")
public class Pergunta extends Model {

    @Column(name = "ordenacao")
    public int ordenacao;

    @Column(name = "descricao")
    public String descricao;

    @Column(name = "resposta")
    public int resposta;

    @Column(name = "escolhido")
    public int escolhido;

    @Column(name = "resultado")
    public String resultado;



    public static void inserir() {
        deletePergunta();
        Pergunta pergunta;
        pergunta = new Pergunta();
        pergunta.descricao = "A linguagem oficial para desenvolvimento Android Nativo pela Google é a Kotlin";
        pergunta.resposta = 1;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 1;
        pergunta.save();

        pergunta = new Pergunta();
        pergunta.descricao = "O processo de publicação do aplicativo na Google Play é gratuito";
        pergunta.resposta = 0;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 2;
        pergunta.save();

        pergunta = new Pergunta();
        pergunta.descricao = "O Brasil possui uma população de quase 210 milhões";
        pergunta.resposta = 1;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 3;
        pergunta.save();

        pergunta = new Pergunta();
        pergunta.descricao = "Flutter é uma dos frameworks de desenvolvimento mobile";
        pergunta.resposta = 1;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 4;
        pergunta.save();

        pergunta = new Pergunta();
        pergunta.descricao = "A linguagem de programação do Flutter é o Dart";
        pergunta.resposta = 1;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 5;
        pergunta.save();

        pergunta = new Pergunta();
        pergunta.descricao = "O Flutter possui interoperabilidade e pode ter projetos em Java e Dart";
        pergunta.resposta = 0;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 6;
        pergunta.save();

        pergunta = new Pergunta();
        pergunta.descricao = "React-Native é uma plataforma para desenvolvimento de aplicativos móveis";
        pergunta.resposta = 1;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 7;
        pergunta.save();

        pergunta = new Pergunta();
        pergunta.descricao = "O Kotlin possui interoperabilidade oque possibilita implementar projetos em Java e Kotlin";
        pergunta.resposta = 1;
        pergunta.escolhido = -1;
        pergunta.ordenacao = 8;
        pergunta.save();


    }


    public Integer qtd_total(){
        int qtd_total = 0;
        Cursor c = ActiveAndroid.getDatabase().rawQuery("SELECT count(*) as qtd_total from pergunta",null);
        if (c.moveToFirst()) {
            qtd_total = c.getInt(c.getColumnIndex("qtd_total"));
        }
        return qtd_total;
    }

    public Integer qtd_acerto(){
        int qtd_total = 0;
        Cursor c = ActiveAndroid.getDatabase().rawQuery("SELECT count(*) as qtd_total from pergunta where resultado = 'acertou'",null);
        if (c.moveToFirst()) {
            qtd_total = c.getInt(c.getColumnIndex("qtd_total"));
        }
        return qtd_total;
    }

    public Integer qtd_erros(){
        int qtd_total = 0;
        Cursor c = ActiveAndroid.getDatabase().rawQuery("SELECT count(*) as qtd_total from pergunta where resultado = 'errou'",null);
        if (c.moveToFirst()) {
            qtd_total = c.getInt(c.getColumnIndex("qtd_total"));
        }
        return qtd_total;
    }


    public Pergunta find(int ordenacao) {
        return new Select().from(Pergunta.class).where("ordenacao = ?", ordenacao).executeSingle();
    }

    public static void deletePergunta() {
        List<Pergunta> perguntas = new Select().from(Pergunta.class).execute();
        for (Pergunta pergunta : perguntas) {
            Pergunta.delete(Pergunta.class, pergunta.getId());
        }
    }


}

