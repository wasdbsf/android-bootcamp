package com.example.desafio;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class resultado extends AppCompatActivity implements View.OnClickListener {
    TextView tvAcertou, TvErrou, TvTotal;
    Button btn_reiniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);
        inicializa_variavel();

    }

    public void inicializa_variavel() {
        tvAcertou = (TextView) findViewById(R.id.tv_acertou);
        TvErrou = (TextView) findViewById(R.id.tv_errou);
        TvTotal = (TextView) findViewById(R.id.tv_total);
        Integer qtd_total = new Pergunta().qtd_total();
        Integer qtd_acerto = new Pergunta().qtd_acerto();
        Integer qtd_erro = new Pergunta().qtd_erros();

        tvAcertou.setText(String.valueOf(qtd_acerto));
        TvErrou.setText(String.valueOf(qtd_erro));
        TvTotal.setText(String.valueOf(qtd_total));

        btn_reiniciar = (Button) findViewById(R.id.btn_reiniciar);
        btn_reiniciar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_reiniciar) {
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}